<?php
/**
 * Inits the add-on when enabled by exchange
 *
 * @since 1.0.0
 * @package IT_Exchange_Related_Products
*/


/**
 * WP Hooks
*/
include( 'lib/hooks.php' );
include( 'lib/functions.php' );

/**
 * Template functions
*/
include( 'lib/template-functions.php' );

/**
 * Load the related products settings metabox class
*/
if ( is_admin() ) {
	include( 'lib/class.related-products.php' );
}