<?php
/*
 * Plugin Name: iThemes Exchange - Related Products
 * Version: 1.0.0
 * Description: Display related products
 * Plugin URI: http://tywayne.com
 * Author: Ty Carlson
 * Author URI: http://tywayne.com
 * Text Domain: exchange-addon-related-products
*/

/**
 * Define the version number
 *
 * @since 1.0.0
*/
define( 'IT_Exchange_Related_Products_Version', '1.0.0' );

/**
 * This registers our plugin as an exchange addon
 *
 * @since 1.0.0
 *
 * @return void
*/
function it_exchange_register_related_products_addon() {
	$options = array(
		'name'              => __( 'Related Products', 'exchange-addon-related-products' ),
		'description'       => __( 'Display related products based on single product pages.', 'exchange-addon-related-products' ),
		'author'            => 'Ty Carlson',
		'author_url'        => 'http://tywayne.com/',
		'icon'              => ITUtility::get_url_from_file( dirname( __FILE__ ) . '/lib/images/icon.png' ),
		'file'              => dirname( __FILE__ ) . '/init.php',
		'category'          => 'product-feature',
		'basename'          => plugin_basename( __FILE__ ),
	);
	it_exchange_register_addon( 'exchange-related-products', $options );
}
add_action( 'it_exchange_register_addons', 'it_exchange_register_related_products_addon' );
