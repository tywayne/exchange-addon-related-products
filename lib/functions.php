<?php

/**
 * Build data to be sent to Backbone on page load
 *
 * @since 1.0.0
 * @param int $post_id - Post ID of current post
 *
 * @return array - array of exchange products and data to be sent to Backbone | id, title, related
*/
if ( !function_exists('exchange_related_products_get_product_json' ) ) {
	function exchange_related_products_get_product_json( $post_id ) {
		$products = it_exchange_get_products( array( 'posts_per_page' => -1 ) );
		$related_ids = get_post_meta( $post_id, '_it_exchange_related_product_ids', true );
		
		//return $products;
		$response = array();
		
		foreach ( $products as $product ) {
			if ( $product->ID != $post_id ) {
				$product_data = array(
					'id'      => $product->ID,
					'title'   => $product->post_title,
					'related' => false
				);
				
				if ( in_array( $product->ID, (array) $related_ids ) ) {
					$product_data['related'] = true;
				}
				
				$response[] = $product_data;
			}
		}
		
		return $response ;
	}
}


/**
 * Create a query for related products to be used for a loop in the template
 *
 * @since 1.0.0
 *
 * @return object - WP_Query object containing related posts
*/
if ( !function_exists('exchange_related_products_get_related_products_query' ) ) {
	function exchange_related_products_get_related_products_query() {
		global $post;
		$related_ids = get_post_meta( $post->ID, '_it_exchange_related_product_ids', true );
		
		if ( empty( $related_ids ) )
			return false;
		
		$args = array(
			'post_type' => 'it_exchange_prod',
			'post__in'  => get_post_meta( $post->ID, '_it_exchange_related_product_ids', true )
		);
		
		return new WP_Query( $args );
	}
}
