<?php
/**
 * Enqueues scripts/styles to the product page
 *
 * @since 1.0.0
 *
 * @return void
*/
function it_exchange_related_products_addon_enqueue_scripts() {
	// enqueue stylesheet on product page
	if ( it_exchange_is_page( 'product' ) ) {
		wp_enqueue_style( 'it-exchange-related-products-frontend', ITUtility::get_url_from_file( dirname( __FILE__ ) ) . '/css/exchange-related-products-frontend.css' );
	}
}
add_action( 'wp_enqueue_scripts', 'it_exchange_related_products_addon_enqueue_scripts' );

/**
 * Enqueues admin scripts to WordPress Dashboard
 *
 * @since 1.0.0
 *
 * @param string $hook_suffix WordPress passed variable
 * @return void
*/
function it_exchange_related_products_addon_admin_wp_enqueue_scripts( $hook_suffix ) {
	global $post;
	
	if ( isset( $_REQUEST['post_type'] ) ) {
		$post_type = $_REQUEST['post_type'];
	} else {
		if ( isset( $_REQUEST['post'] ) )
			$post_id = (int) $_REQUEST['post'];
		elseif ( isset( $_REQUEST['post_ID'] ) )
			$post_id = (int) $_REQUEST['post_ID'];
		else
			$post_id = 0;
		
		if ( $post_id )
			$post = get_post( $post_id );
		
		if ( isset( $post ) && !empty( $post ) )
			$post_type = $post->post_type;
	}
	
	wp_register_script( 'it-exchange-related-products-admin', ITUtility::get_url_from_file( dirname( __FILE__ ) ) . '/js/exchange-related-products-admin.js', array( 'jquery', 'wp-backbone', 'underscore' ) );
	wp_register_script( 'it-exchange-related-products-select2', ITUtility::get_url_from_file( dirname( __FILE__ ) ) . '/js/select2.min.js', array( 'jquery' ) );
	wp_register_style( 'it-exchange-related-products-admin-style', ITUtility::get_url_from_file( dirname( __FILE__ ) ) . '/css/exchange-related-products-admin.css' );
	wp_register_style( 'it-exchange-related-products-select2-style', ITUtility::get_url_from_file( dirname( __FILE__ ) ) . '/css/select2.min.css' );


	if ( isset( $post_type ) && 'it_exchange_prod' === $post_type && ( 'post.php' == $hook_suffix || 'post-new.php' == $hook_suffix ) ) {
		wp_localize_script( 'it-exchange-related-products-admin' , 'productData', exchange_related_products_get_product_json( $post->ID ) );
		wp_enqueue_script( 'it-exchange-related-products-admin' );
		wp_enqueue_script( 'it-exchange-related-products-select2' );
		wp_enqueue_style( 'it-exchange-related-products-admin-style' );
		wp_enqueue_style( 'it-exchange-related-products-select2-style' );
	}

}
add_action( 'admin_enqueue_scripts', 'it_exchange_related_products_addon_admin_wp_enqueue_scripts' );

/**
 * Builds JSON requests for backbone requests
 *
 * @since 1.0.0
 *
 * @return void
*/
function it_exchange_related_products_json_api() {

	$post_id = empty( $_REQUEST['post_id'] ) ? false : $_REQUEST['post_id'];
	
	if ( empty( $post_id ) )
		return false;
	
	$products = it_exchange_get_products();
	$related = get_post_meta( $post_id, '_it_exchange_related_product_ids', true );
	$response = array();
	
	foreach ( $products as $product ) {
		$product_data          = new stdClass();
		$product_data->id      = $product->ID;
		$product_data->title   = $product->post_title;
		$product_data->related = false;
		
		if ( in_array( $product->ID, (array) $related ) )
			$product_data->related = true;
		
		$response[] = $product_data;
	}
	
	die( json_encode( $response ) );
	
	return false;
}

add_action( 'wp_ajax_it-exchange-related-products-json-api', 'it_exchange_related_products_json_api' );
add_action( 'wp_ajax_nopriv_it-exchange-variants-json-api', 'it_exchange_related_products_json_api' );