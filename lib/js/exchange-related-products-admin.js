(function($){

	/** 
	 * Init our backbone app
	 *  - we call app.start( productData ) on $(document).ready()
	 */
	var app = {

		/**
		 *  Build our initial collections and pass to views
		 *
		 * @param {object} data - array of product data objects. each product object has the same properties as app.Models.Product
		 */
		start: function(data) {
			this.data = data;
			this.originalRelatedProducts = [];

			// look through data, and set hidden input value for all original related products
			_.each( this.data, function(element) {
				if ( element.related ) {
					app.originalRelatedProducts.push( element.id );
				}
			});
			$('#it_exchange_related_products_original_input').val( app.originalRelatedProducts );

			// setup collection of all products
			this.products = new this.Collections.Products( data );

			// setup view of Available Products
			app.AvailableProductsView = new this.Views.Products({ collection: this.products });
			app.AvailableProductsView.render();

			//setup view of Related Products
			app.RelatedProductsView = new this.Views.RelatedProducts({ collection: this.products });
			app.RelatedProductsView.render();
		}
	};

	/**
	 * MODELS
	 */
	app.Models = {};
	app.Models.Product = Backbone.Model.extend({
		defaults: {
			id:      '',
			title:   '',
			related: false,
		}
	});

	/**
	 * COLLECTIONS
	 */
	app.Collections = {};
	app.Collections.Products = Backbone.Collection.extend({
		model: app.Models.Product,

		related: function() {
			return this.where({related: true});
		},

		available: function() {
			return this.where({related: false});
		}
	});

	/**
	 * VIEWS
	 */
	app.Views = {};

	/** Available Products Dropdown View */
	app.Views.Products = wp.Backbone.View.extend({
		initialize: function() {
			this.listenTo( this.collection, 'all', this.render );
			this.$el = $('.it-exchange-available-product-container');
		},

		events: {
			'click .add-new-related-product' : 'addRelatedProduct',
		},

		addOne: function(product) {
			var view = new app.Views.Product({model: product});
			$("#it_exchange_add_new_related_product").append( view.render().el );
		},

		render: function() {
			$("#it_exchange_add_new_related_product").html('');

			if ( this.collection.available().length ) {
				$('.it-exchange-available-product-dropdown').removeClass('hidden');
				$('.it-exchange-no-available-products').addClass('hidden');
				this.collection.each( this.addOne, this );
			}
			else {
				$('.it-exchange-available-product-dropdown').addClass('hidden');
				$('.it-exchange-no-available-products').removeClass('hidden');
			}
		},

		addRelatedProduct: function() {
			var selected = $('#it_exchange_add_new_related_product option:selected'),
				selected_id = selected.data('id'),
				m = this.collection.get(selected_id);

			m.set( 'related', true );

			app.$productSelect.select2();
		}
	});

	/** Single Available Product View */
	app.Views.Product = wp.Backbone.View.extend({
		template: wp.template( "available_product_template" ),

		initialize: function() {
			this.listenTo( this.model, 'change', this.render);
			this.el = $(".it-exchange-available-product-container");
		},

		render: function() {
			this.setElement( this.template( this.model.toJSON() ) );
			return this;
		},
	});

	/** Related Products List View */
	app.Views.RelatedProducts = wp.Backbone.View.extend({
		className: "related-product",
		el: $(".it-exchange-related-product-list-container"),
		initialize: function() {
			this.listenTo( this.collection, 'all', this.render );
		},

		addOne: function(product) {
			var view = new app.Views.RelatedProduct({model: product});
			$(".it-exchange-related-product-list").append( view.render().el );
		},

		render: function() {
			this.HiddenInputValue = this.getHiddenInputValue();
			$('#it_exchange_related_products_input').val(this.HiddenInputValue);

			$(".it-exchange-related-product-list").html('');

			if ( this.collection.related().length ) {
				$(".it-exchange-related-product-list").removeClass('hidden');
				$('.it-exchange-no-related-products').addClass('hidden');
				this.collection.each( this.addOne, this );
			}
			else {
				$(".it-exchange-related-product-list").addClass('hidden');
				$('.it-exchange-no-related-products').removeClass('hidden');
			}
		},

		getHiddenInputValue: function() {
			var value = [], related;

			related = this.collection.where({ related: true });

			$.each( related, function(index) {
				value.push( related[index]['id'] );
			});

			value = value.join(' ');

			return value;
		}
	});

	/** Single Related Product View */
	app.Views.RelatedProduct = wp.Backbone.View.extend({
		template: wp.template( "related_product_template" ),

		events: {
			'click .remove-related-product': 'removeRelatedProduct'
		},

		initialize: function() {
			this.listenTo(this.model, 'change', this.render);
		},

		render: function() {
			this.setElement( this.template( this.model.toJSON() ) );
			return this;
		},

		removeRelatedProduct: function() {
			this.model.set( 'related', false );
			app.$productSelect.select2();
		},
	});

	$(document).ready( function() {
		// init the backbone app
		app.start( productData );

		// init select2 for UI
		app.$productSelect = $('#it_exchange_add_new_related_product');
		app.$productSelect.select2();
	});

})(jQuery);