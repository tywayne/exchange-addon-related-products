<?php
/**
 * This sets up a meta box for the product to allow for adding related products
 *
 * @since 1.0.0
 * @package IT_Exchange_Related_Products
*/
class IT_Exchange_Related_Products_Meta_Box {

	/**
	 * Class constructor. Registers hooks
	 *
	 * @since 1.0.0
	 * @return void
	*/
	function IT_Exchange_Related_Products_Meta_Box() {
		if ( is_admin() ) {
			add_action( 'it_exchange_product_metabox_callback', array( $this, 'register_related_products_meta_box' ) );
			add_action( 'it_exchange_save_product', array( $this, 'update_related_products' ) );
		}
	}
	
	/**
	 * Register's the Related Product Metabox
	 *
	 * @since 1.0.0
	 * @return void
	*/
	function register_related_products_meta_box( $post ) {
		add_meta_box( 'it_exchange_related_products', __( 'Related Products', 'exchange-addon-related-products' ), array( $this, 'print_meta_box' ), $post->post_type, 'side' );
	}
	
	/**
	 * Prints the contents of the metabox
	 *
	 * @since 1.0.0
	 * @return void
	*/
	function print_meta_box( $post ) {
	?>	
		<div class="it-exchange-related-product-container">
			<div class="it-exchange-related-product-list-container">
				<h2>Related Products</h2>
				<ul class="it-exchange-related-product-list"></ul>
				
				<p class="it-exchange-no-related-products">
					<?php _e('You have not added any related products. Choose one below and click \'Add Related Product\'.', 'exchange-addon-related-products' ); ?>
				</p>
			</div>
			<div class="it-exchange-available-product-container">
				<h2>Add New Related Product</h2>
				
				<div class="it-exchange-available-product-dropdown">
					<select id="it_exchange_add_new_related_product"></select>
					<a class="button-secondary add-new-related-product">Add Related Product</a>
				</div>
				
				<p class="it-exchange-no-available-products hidden">
					<?php _e('All available products have been added.', 'exchange-addon-related-products' ); ?>
				</p>
				
				<input type="hidden" id="it_exchange_related_products_input" name="it-exchange-related-products" value="" />
				<input type="hidden" id="it_exchange_related_products_original_input" name="it-exchange-original-related-products" value="" />
			</div>
			
			<!-- Related Products Template -->
			<script id="tmpl-related_product_template" type="text/html">
				<# if ( data.related ) { #>
					<li>{{ data.title }}<span class="remove-related-product" data-product_id="{{ data.id }}">&times;</span></li>
				<# } #>
			</script>
			
			<!-- Available Products Template -->
			<script id="tmpl-available_product_template" type="text/html">
				<# if ( ! data.related ) { #>
					<option data-id="{{ data.id }}" data-title="{{ data.title ]}">{{ data.title }}</option>
				<# } #>
			</script>
		</div>
		<?php
	}
	
	/**
	 * Updates the post_meta that holds the related posts
	 *
	 * @since 1.0.0
	 * @return void
	*/
	function update_related_products( $post ) {
		
		// Ensure we have a WP post object or return
		if ( ! is_object( $post ) )
			$post = get_post( $post );
		if ( empty( $post->ID ) )
			return;
		if ( ! $product = it_exchange_get_product( $post ) )
			return;
		if ( ! $product->ID )
			return;
		
		// If there is an updated related product id in the POST array, use that.
		$related_product_ids = ! empty( $_POST['it-exchange-related-products'] ) ? explode( ' ', $_POST['it-exchange-related-products'] ) : array();
		$original_related_product_ids = ! empty( $_POST['it-exchange-original-related-products'] ) ? explode( ',', $_POST['it-exchange-original-related-products'] ) : array();
		
		// If there were items removed, then adjust the removed items meta
		$removed = array_diff( $original_related_product_ids, $related_product_ids );
		
		if ( $removed ) {
			foreach ( $removed as $id ) {
				// get post meta for each product id
				$related_products = get_post_meta( $id, '_it_exchange_related_product_ids', true );
				
				// if get_post_meta returns an empty string, make it an empty array
				if ( empty( $related_products ) )
					$related_products = array();
				
				$key = array_search($post->ID, $related_products);
				
				if ( false !== $key ){
					unset($related_products[$key]);
				}
				
				// update related post meta with the updated array
				update_post_meta( $id, '_it_exchange_related_product_ids', $related_products );
			}
		}
		
		// If we have related product ids, update
		if ( $related_product_ids ) {
			// iterate over that array and update post meta for the products
			foreach ( $related_product_ids as $id ) {
				// get post meta for each product id
				$related_products = get_post_meta( $id, '_it_exchange_related_product_ids', true );
				
				// if get_post_meta returns an empty string, make it an empty array
				if ( empty( $related_products ) )
					$related_products = array();
				
				// if the current post-ID is not in that array, then add it
				if ( ! in_array( $post->ID, (array) $related_products ) )
					array_push( $related_products, (string) $post->ID );
				
				// update related post meta with the updated array
				update_post_meta( $id, '_it_exchange_related_product_ids', $related_products );
			}
			
			// update the current product post meta
			update_post_meta( $post->ID, '_it_exchange_related_product_ids', $related_product_ids );
			
		} else { // if $related_product_ids was empty, then all related products are gone - so we set the post meta to an empty string
			update_post_meta( $post->ID, '_it_exchange_related_product_ids', '' );
		}
	}
}
new IT_Exchange_Related_Products_Meta_Box();
