<?php
/**
 * This file contains functions related to related products frontend templating
 * @since 1.0.0
*/

/**
 * Inserts related products template in advanced section
*/
function it_exchange_related_products_addon_register_template_loop( $elements ) {
	// Splice our template in after extended-description if it is first
	if ( 'extended-description' == $elements[0] )
		$index = 1;
	else
		$index = 0;

	$index = apply_filters( 'it_exchange_get_content_product_product_info_loop_elements_related_products_index', $index );
	array_splice( $elements, $index, 0, 'related-products' );	

	return $elements;
}
add_filter( 'it_exchange_get_content_product_product_advanced_loop_elements', 'it_exchange_related_products_addon_register_template_loop' );

/**
 * Adds our templates directory to the list of directories
 * searched by Exchange
 *
 * @since 1.0.0
 *
 * @param array $template_path existing array of paths Exchange will look in for templates
 * @param array $template_names existing array of file names Exchange is looking for in $template_paths directories
 * @return array
*/
function it_exchange_addon_related_products_register_templates( $template_paths, $template_names ) { 
	// Bail if not looking for one of our templates
	$add_path = false;
	$templates = array(
		'content-product/elements/related-products.php',
	);  
	foreach( $templates as $template ) { 
		if ( in_array( $template, (array) $template_names ) )
			$add_path = true;
	}
	if ( ! $add_path )
		return $template_paths;

	$template_paths[] = dirname( __FILE__ ) . '/templates';
	return $template_paths;
}
add_filter( 'it_exchange_possible_template_paths', 'it_exchange_addon_related_products_register_templates', 10, 2 );
