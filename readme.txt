=== Related Products for iThemes Exchange ===
Contributors: tywayne
Tags: iThemes Exchange, addon, ecommerce
Requires at least: 3.9
Tested up to: 4.1
Stable tag: 1.0.0
License: GPLv2 or later

Manually designate similar products as "related" and display them on a single product screen.

== Description ==

= Show off similar products =
Designate similar products as `Related` and then display them on a single product screen. 
Allows visitors to find other similar products when browsing your store, and help boost sales. 

= Style related products however you'd like =
Adds additional templates to the iThemes Exchange template system which will allow your theme to override the default markup and styling add by this pluing.
Make your single product screen match your existing site perfectly. 

== Installation ==

1. Download and unzip the latest release zip file.
2. If you use the WordPress plugin uploader to install this plugin skip to step 4
3. Upload the entire plugin directory to your `/wp-content/plugins/` directory.
4. Activate the plugin through the 'Plugins' menu in WordPress Administration.

== Screenshots ==


== Changelog ==
